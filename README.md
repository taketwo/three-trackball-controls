# three-trackball-controls

TrackballControls for three.js, copied without change from the [examples](https://github.com/mrdoob/three.js/blob/master/examples/js/controls/TrackballControls.js).

## Usage

Require passing an instance of THREE:

```js
var TrackballControls = require('three-trackball-controls')(THREE)
```

Create a controller object:

```js
controls = new TrackballControls(camera[, domElement])
```

Make sure to call `controls.update()` in your animation/render loop, and also
call `controls.handleResize()` whenever the canvas is resized.
